# Projeto: Mentoria de testes automatizados web com JUnit 5

Este projeto tem por objetivo testar minhas habilidades em TESTES e AUTOMAÇÕES no qual tive que simular uma compra com sucesso no site "http://automationpractice.com/index.php?"

**Tecnologias Utilizadas**

Este projeto conta com as seguintes tecnologias:  

- Java (Ver. 1.8.0)
- JUnit5 (Ver. 5.7.1)
- Maven  (Ver. 3.6.3)
- ExtentReport (Ver. 4.0.9)
- Java Faker (Ver. 1.0.2)
- WebDriveManager (Ver. 4.3.1)

**Padrões Utilizados**

**Main/Java**

- **Framework** : Pasta onde se encontram classes de abstracao.

- **Utils** :  Pasta onde ficam classes com métodos auxiliares para serem reutilizadas em outras classes.

**Main/Resources**

Pasta onde se encontram arquivos de inputs para execução dos testes.


**Test/Java**

- **PageObjects** : Pasta onde se encontram classes de mapeamento dos WebElements.

- **Tasks** : Pasta onde se encontram classes de iteração com os WebElements.

- **CaseTests** : Pasta onde se encontram classes e métodos de Testes.



