package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utils.Waits;

public class IndexPage {
	private WebDriver driver;
	private Waits wait;
	
	public IndexPage(WebDriver driver) {
		wait = new Waits(driver);
		this.driver = driver;
	}
	
	public WebElement getProduct() {
		return wait.ElementVisibility(By.xpath("//h5[@itemprop='name']/a[@title='Blouse']"));
	}
	
}
