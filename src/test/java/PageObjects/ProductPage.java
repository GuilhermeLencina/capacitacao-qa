package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utils.Waits;

public class ProductPage {
	private WebDriver driver;
	private Waits wait;
	
	public ProductPage(WebDriver driver) {
		wait = new Waits(driver);
		this.driver = driver;
	}
	
	public WebElement addToCart() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"add_to_cart\"]/button/span"));
	}
	
	public WebElement checkout() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span"));
	}
	
	public WebElement confirmProductCart() {
		return wait.ElementVisibility(By.id("total_price"));
	}
	
	public WebElement confirmPurchase() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"center_column\"]/p[2]/a[1]/span"));
	}
}
