package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Utils.Waits;

public class AccountPage {
	private WebDriver driver;
	private Waits wait;

	public AccountPage(WebDriver driver) {
		wait = new Waits(driver);
		this.driver = driver;
	}

	public WebElement addEmailCreatAccount() {
		return wait.ElementVisibility(By.id("email_create"));
	}

	public WebElement confirmEmailCreateAccount() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"SubmitCreate\"]/span"));
	}
	
	public WebElement gender() {
		return wait.ElementVisibility(By.id("id_gender1"));
	}
	
	public WebElement firstName() {
		return wait.ElementVisibility(By.id("customer_firstname"));
	}
	
	public WebElement lastName() {
		return wait.ElementVisibility(By.id("customer_lastname"));
	}
	
	public WebElement password() {
		return wait.ElementVisibility(By.id("passwd"));
	}
	
	public Select dateOfBirthDays() {
		return new Select(driver.findElement(By.id("days")));
	}
	
	public Select dateOfBirthMonths() {
		return new Select(driver.findElement(By.id("months")));
	}
	
	public Select dateOfBirthYears() {
		return new Select(driver.findElement(By.id("years")));
	}
	
	public WebElement address() {
		return wait.ElementVisibility(By.id("address1"));
	}
	
	public WebElement city() {
		return wait.ElementVisibility(By.id("city"));
	}
	
	public Select state() {
		return new Select(driver.findElement(By.id("id_state")));
	}
	
	public WebElement postal() {
		return wait.ElementVisibility(By.id("postcode"));
	}
	
	public Select country() {
		return new Select(driver.findElement(By.id("id_country")));
	}
	
	public WebElement phone() {
		return wait.ElementVisibility(By.id("phone"));
	}
	
	public WebElement mobilePhone() {
		return wait.ElementVisibility(By.id("phone_mobile"));
	}
	
	public WebElement address2() {
		return wait.ElementVisibility(By.id("alias"));
	}
	
	public WebElement register() {
		return wait.ElementVisibility(By.id("submitAccount"));
	}
	
	
	
	
	

}