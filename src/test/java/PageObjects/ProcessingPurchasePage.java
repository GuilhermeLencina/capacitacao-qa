package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utils.Waits;

public class ProcessingPurchasePage {
	private WebDriver driver;
	private Waits wait;

	public ProcessingPurchasePage(WebDriver driver) {
		wait = new Waits(driver);
		this.driver = driver;
	}
	
	public WebElement informationCity() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"address_delivery\"]/li[4]"));
	}

	public WebElement informationState() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"address_delivery\"]/li[5]"));
	}
	
	public WebElement confirmCheckout1() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"center_column\"]/form/p/button/span"));		
	}
	
	public WebElement checkBox() {
		return wait.ElementVisibility(By.id("uniform-cgv"));		
	}
	
	public WebElement confirmCheckout2() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"form\"]/p/button/span"));		
	}
	
	public WebElement finalValue() {
		return wait.ElementVisibility(By.id("total_price"));
	}
	
	public WebElement formPayment() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a"));
	}
	
	public WebElement order() {
		return wait.ElementVisibility(By.xpath("//*[@id=\"cart_navigation\"]/button/span"));
	}
}
