package Tasks;

import org.openqa.selenium.WebDriver;

import Framework.JavaFaker;
import PageObjects.AccountPage;

public class AccountTask {
	private static WebDriver driver;
	private AccountPage account;
	private JavaFaker faker;

	public AccountTask(WebDriver driver) {
		this.driver = driver;
		account = new AccountPage(this.driver);
		faker = new JavaFaker();
	}

	public void EmailAccount() {
		account.addEmailCreatAccount().sendKeys(faker.getEmail());
	}

	public void accountCreate() {
		account.confirmEmailCreateAccount().click();
		account.gender().click();
		account.firstName().sendKeys(faker.getFirstName());
		account.lastName().sendKeys(faker.getLastName());
		account.password().sendKeys(faker.getPassword());
		
		account.dateOfBirthDays().selectByValue("1");
		account.dateOfBirthMonths().selectByValue("2");
		account.dateOfBirthYears().selectByValue("2000");
		
		account.address().sendKeys(faker.getStreetAddress());
		account.city().sendKeys("Gua�ba");
		account.state().selectByValue("3");
		account.postal().sendKeys("00000");
		account.country().selectByValue("21");	
	
		account.phone().sendKeys("987654321");		
		account.mobilePhone().sendKeys("123456789");	
		account.address2().sendKeys(faker.getStreetAddress());		
		
		account.register().click();
	}
}
