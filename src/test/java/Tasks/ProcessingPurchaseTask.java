package Tasks;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import Framework.JavaFaker;
import Framework.Report;
import Framework.Screenshot;
import PageObjects.AccountPage;
import PageObjects.ProcessingPurchasePage;

public class ProcessingPurchaseTask {
	private static WebDriver driver;
	private ProcessingPurchasePage processing;
	private AccountPage account;
	
	public ProcessingPurchaseTask(WebDriver driver) {
		this.driver = driver;
		processing = new ProcessingPurchasePage(this.driver);
		account = new AccountPage(driver);
	}
	
	public void confirmMyOrder() {
		Assertions.assertEquals(processing.informationCity().getText(),"Gua�ba, Arizona 00000");
		Assertions.assertEquals(processing.informationState().getText(), "United States");
		processing.confirmCheckout1().click();
		processing.checkBox().click();
		processing.confirmCheckout2().click();
		try {
			Assertions.assertEquals(processing.finalValue().getText(), "$29.00");
			Report.log(Status.PASS, "The total amount is correct!", Screenshot.capture(driver));
		}catch(Exception e) {
			Report.log(Status.FAIL, "The total amount is not correct!", Screenshot.capture(driver));
		}
		
		processing.formPayment().click();
		processing.order().click();
	}
}
