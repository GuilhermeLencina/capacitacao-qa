package Tasks;

import org.openqa.selenium.WebDriver;

import PageObjects.IndexPage;

public class IndexTask {
	private static WebDriver driver;
	private static IndexPage index;
	
	public IndexTask(WebDriver driver) {
		this.driver = driver;
		index = new IndexPage(this.driver);		
	}
	
	public void viewProduct() {
		index.getProduct().click();
	}
}
