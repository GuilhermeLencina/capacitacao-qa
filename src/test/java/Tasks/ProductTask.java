package Tasks;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import Framework.Report;
import Framework.Screenshot;
import PageObjects.ProductPage;

public class ProductTask {
	private static WebDriver driver;
	private static ProductPage product;
	
	public ProductTask(WebDriver driver) {
		this.driver = driver;
		product = new ProductPage(this.driver);
	}
	
	public void addToCartProduct() {
		product.addToCart().click();
		product.checkout().click();
		try {
			Assertions.assertEquals(product.confirmProductCart().getText(), "$29.00");
			Report.log(Status.PASS, "The product was added to the cart success!", Screenshot.capture(driver));
		}catch(Exception e) {
			Report.log(Status.FAIL, "The product was not added to the cart!", Screenshot.capture(driver));
		}
		
		product.confirmPurchase().click();
	}
}
