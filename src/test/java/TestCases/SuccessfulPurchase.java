package TestCases;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import Framework.Report;
import Framework.TestBase;
import Framework.TypeReport;
import Tasks.AccountTask;
import Tasks.IndexTask;
import Tasks.ProcessingPurchaseTask;
import Tasks.ProductTask;

public class SuccessfulPurchase extends TestBase {
	private WebDriver driver = this.getDriver();
	IndexTask index = new IndexTask(driver);
	ProductTask product = new ProductTask(driver);
	AccountTask account = new AccountTask(driver);
	ProcessingPurchaseTask processing = new ProcessingPurchaseTask(driver);
	

	@Test
	public void successfulPurchase() {
		try {
			Report.createTest("Make Sucessful Purchase!", TypeReport.INDIVIDUAL );
			
			index.viewProduct();
			product.addToCartProduct();
			account.EmailAccount();			
			account.accountCreate();
			processing.confirmMyOrder();
			
		}catch(Exception e) {
			Report.log(Status.ERROR, e.getMessage());
		}
		

	}
}
