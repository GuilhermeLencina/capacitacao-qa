package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class FileManager {
	private static final String DIR_PATH_PROPERTIES = System.getProperty("user.dir") + File.separator + "src"
			+ File.separator + "main" + File.separator + "resources" + File.separator + "Properties" + File.separator;

	public Properties getProperties(String fileName) throws IOException {
		InputStream inputStream = null;
		Properties prop = new Properties();
		try {
			File file = new File(DIR_PATH_PROPERTIES + fileName + ".properties");
			inputStream = new FileInputStream(file);
			prop.load(inputStream);
			return prop;
		} catch (Exception e) {
			System.out.println("Passing the exception " + e.getMessage());
		} finally {
			inputStream.close();
		}
		return prop;
	}

	public void setProperties(String fileName, String propKey, String propValue) throws IOException {
		Properties prop = getProperties(fileName);
		try {
			OutputStream outputStream = new FileOutputStream(DIR_PATH_PROPERTIES + fileName + ".properties");
			prop.setProperty(propKey, propValue);
			prop.store(outputStream, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
