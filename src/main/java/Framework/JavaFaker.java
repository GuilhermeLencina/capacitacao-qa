package Framework;

import com.github.javafaker.Faker;

public class JavaFaker {
	
	private Faker faker;
	private String firstName;
	private String lastName;
	private String zipCode;
	private String email;
	private String firstNameAddress;
	private String numPhone;

	public JavaFaker() {
        faker = new Faker();
    }

	public String getFirstName() {
		firstName = faker.name().firstName();
		return firstName;
	}

	public String getLastName() {
		lastName = faker.name().lastName();
		return lastName;
	}

	public String getZipCode() {
		zipCode = faker.address().zipCode();
		return zipCode;
	}

	public String getEmail() {
		email = faker.internet().emailAddress();
		return email;
	}

	public String getFirstNameAddress() {
		firstNameAddress = faker.address().firstName();
		return firstNameAddress;
	}

	public String getLastNameAddress() {
		return faker.address().lastName();
	}

	public String getStreetAddress() {
		return faker.address().streetAddress();
	}

	public String getCityName() {
		return faker.address().cityName(); 
	}

	public String getNumCellPhone() {
		return faker.phoneNumber().phoneNumber().formatted("#########");
		
	}

	public String getNumPhone() {
		numPhone = faker.phoneNumber().phoneNumber().formatted("#########");
		return numPhone;
	}

	public String getPassword() {
		return faker.internet().password();
	}
}
