package Framework;

import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;

import Utils.FileManager;

public class TestBase extends DriverManager{
	private static WebDriver driver;
	private static FileManager fileManager = new FileManager();
	
	public static WebDriver getDriver() {
		driver = getDriver(TypeDriver.CHROME);
		return driver;
	}
	
	@BeforeEach
	public void setUp() throws IOException {
		String index = fileManager.getProperties("url").getProperty("url.index");
		getDriver().get(index);
	}	
	
	@AfterEach
	public void closeDriver() {
		Report.close();
		shutdownDriver();
	}
}
