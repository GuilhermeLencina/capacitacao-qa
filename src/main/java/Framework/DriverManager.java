package Framework;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverManager {
	private static WebDriver driver;

	private static WebDriver getDriverManager(TypeDriver browser) {
		switch (browser) {

		case CHROME:
			WebDriverManager.chromedriver().setup();
			ChromeOptions optionsChrome = new ChromeOptions();
			optionsChrome.addArguments("--start-maximized");
			driver = new ChromeDriver(optionsChrome);
			break;

		case FIREFOX:
			WebDriverManager.firefoxdriver().setup();
			FirefoxOptions optionsFirefox = new FirefoxOptions();
			optionsFirefox.addArguments("--start-maximized");
			driver = new FirefoxDriver(optionsFirefox);
			break;

		case HEADLESS:
			WebDriverManager.chromedriver().setup();
			ChromeOptions headless = new ChromeOptions();
			headless.addArguments("--headless");
			driver = new ChromeDriver(headless);
			break;

		case IE:
			WebDriverManager.iedriver().setup();
			MutableCapabilities capabilities = new MutableCapabilities();
			capabilities.setCapability(CapabilityType.BROWSER_NAME, BrowserType.IE);
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver = new InternetExplorerDriver();
			break;

		default:
			break;
		}

		return driver;
	}

	public static WebDriver getDriver(TypeDriver browser) {
		if (driver == null) {
			driver = getDriverManager(browser);
		}
			return driver;
	}
	
	public static void shutdownDriver() {
		 if (driver != null) {

	            driver.quit();
	            driver = null;
	        }
	}
}
